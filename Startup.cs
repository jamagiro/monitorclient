﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MonitorClient.Startup))]
namespace MonitorClient
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
