﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace MonitorClient.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Monitor()
        {

            
            
            try
            {
                Pais.ws_pais ws = new Pais.ws_pais();
                ws.activo();
                Console.WriteLine("Se instancia pais");
                ViewBag.statuspais = "running";
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Error");
                ViewBag.statuspais = "down";
                
            }

            try
            {
                Ciudad.ws_ciudad ws = new Ciudad.ws_ciudad();
                ws.activo();
                Console.WriteLine("Se instancia ciudad");
                ViewBag.statusciudad = "running";
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Error");
                ViewBag.statusciudad = "down";

            }

            try
            {
                Vuelo.ws_vuelo ws = new Vuelo.ws_vuelo();
                ws.activo();
                Console.WriteLine("Se instancia vuelo");
                ViewBag.statusvuelo = "running";
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Error");
                ViewBag.statusvuelo = "down";

            }

            try
            {
                Hoteles.ws_hoteles ws = new Hoteles.ws_hoteles();
                ws.activo();
                Console.WriteLine("Se instancia hoteles");
                ViewBag.statushoteles = "running";
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Error");
                ViewBag.statushoteles = "down";

            }


            try
            {
                Categorias.ws_categorias ws = new Categorias.ws_categorias();
                ws.activo();
                Console.WriteLine("Se instancia categorias");
                ViewBag.statuscategorias = "running";
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Error");
                ViewBag.statuscategorias = "down";

            }

            return View();
        }
    }
}